# Changelog: #

## 1.2 ##

### Fixed ###

- Now it properly cleans the terminal before run

## 1.1 ##

### Fixed ###

- Updated script to properly remove admin passwords

## 1.0 ##
Initial release