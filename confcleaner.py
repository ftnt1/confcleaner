#!/usr/bin/env python3

"""confcleaner.py: This script will clean up a config file by removing unnecessary comments, admin passwords, UUID's etc so it is easier to troubleshoot"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.2"
__status__ = "Production"

import os

BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal
os.system('cls' if os.name == 'nt' else 'clear')

print(GREEN + '\n\n\n\n\nWelcome!\nThis is FortiOS Configuration Cleaner v1.0\n' + NORM)


# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .conf to list "cfgfiles"
for file in dirs:
    if ".conf" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
else:
    print('\nOnly one config file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()

# removing comments, passwords, descriptions and UUID's

cfg = [line for line in cfg if not line.startswith('        set comment')
       and not line.startswith('        set passwd ')
       and not line.startswith('        set password ')
       and not line.startswith('        set description ')
       and not line.startswith('        set uuid')]

f = open(cfgfile + '.out', mode='w')
f.writelines(cfg)
f.close()