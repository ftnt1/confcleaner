# README #

FortiOS config file cleaner

## Files ##

### confcleaner.py ###

This script will clean up a config file by removing unnecessary comments, passwords, UUID's etc so it is easier to troubleshoot

Run it from within the directory containing firewall config(s).

It takes no command line arguments

### README.md ###

This file

## Usage ##

Run script in *.conf FortiOS config file location (ex. "config.conf")
It will create new file with .out extension (ex. "config.conf.out")
It will save your eyes from too much pain by removing UUID's, comments / descriptions from policies and objects so it is more readable
It also removes admin passwords